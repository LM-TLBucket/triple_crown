#! /usr/bin/env python3
'''This is the Triple Crown Wordpress app
'''
import argparse
import base64
from datetime import datetime
import html
import os
import re
import requests

WORDPRESS_USERNAME = os.getenv('WORDPRESS_USERNAME')
WORDPRESS_PASSWORD = os.getenv('WORDPRESS_PASSWORD')
WORDPRESS_URL = os.getenv('WORDPRESS_URL')
WP_AUTHSTRING = (f'{WORDPRESS_USERNAME}:{WORDPRESS_PASSWORD}')
WP_ENCODED = base64.b64encode(WP_AUTHSTRING.encode('utf-8'))
WP_AUTHKEY = str(WP_ENCODED, 'utf-8')
WP_BASEURL = f'{WORDPRESS_URL}/wp-json/wp/v2/'
WP_HEADER = {'Authorization': 'Basic %s' % WP_AUTHKEY}
WP_RESTAPI_POSTS = f'{WP_BASEURL}posts'
WP_RESTAPI_COMMENTS = f'{WP_BASEURL}comments'
PARSER = argparse.ArgumentParser()
ACCEPT = ["show", "upload", "comment", "fileload"]


def get_all_posts():
    '''Gets all the Wordpress posts.

    Raises: ValueError if a bad response is received

    Returns: A list of all posts
    '''
    response = requests.get(WP_RESTAPI_POSTS, headers=WP_HEADER)
    if response.status_code != 200:
        raise ValueError('Received bad response from Wordpress; Error Message: %s' % response.json()['message'])  # noqa
    return response.json()


def read_blog_posts(num_posts=1):
    '''Gets the number of specified posts; by default it will return the latest
    blog post.

    Args: num_posts: An integer to indicate the number of blog posts to return.
            Default will return 1 blog post. If num_posts is greater than the
            number of available posts then all available posts will be returned.

    Returns: A list of posts
    '''  # noqa
    list_all_posts = get_all_posts()
    list_ids = []
    list_posts = []
    for n in list_all_posts:
        list_ids.append(n['id'])
    if num_posts > len(list_ids):
        print(f'The number of requested posts ({num_posts}) is greater than the number of actual posts ({len(list_ids)}).')  # noqa
        print('Therefore, all available posts will be returned.\n')
        num_posts = len(list_ids)
    for num in range(0, num_posts):
        post_id = list_all_posts[num]['id']
        date = list_all_posts[num]['date']
        title = html.unescape(list_all_posts[num]['title']['rendered'])
        contents = re.sub(r'\<[^>]*>', r'', html.unescape(list_all_posts[num]['content']['rendered']).rstrip())  # noqa
        result = f'id: {post_id}\ndate: {date}\ntitle: {title}\ncontents: {contents}\n'  # noqa
        list_posts.append(result)
    for post in list_posts:
        print(post)
    return list_posts


def write_blog_post(title, content):
    '''Write a blog post to Wordpress.

    Args: title: A title for the post
          content: The body of the post

    Raises: ValueError if a bad response is received

    Returns: A link to the published post
    '''
    post_details = {
        'date': datetime.isoformat(datetime.now()),
        'title': title,
        'status': 'publish',
        'content': content
    }
    response = requests.post(WP_RESTAPI_POSTS, headers=WP_HEADER, json=post_details)  # noqa
    if response.status_code != 201:
        raise ValueError("Received bad response attempting to POST to Wordpress; Error Message: %s" % response.json()['message'])  # noqa
    result = 'Your post is published on ' + response.json()['link']
    print(result)
    return result


def write_blog_comment(post_id, comment):
    '''Add comment to an existing post.

    Args: post_id: The id of the specific post where comment will be added
          comment: The content of the comment

    Raises: ValueError if a bad response is received

    Returns: A link to the published comment
    '''
    wp_comment_url = WP_RESTAPI_COMMENTS + f'/?post={post_id}'
    comment_details = {
        'date': datetime.isoformat(datetime.now()),
        'content': comment
    }
    response = requests.post(wp_comment_url, headers=WP_HEADER, data=comment_details)  # noqa
    if response.status_code != 201:
        raise ValueError('Received bad response attempting to add a Wordpress comment; Error Message: %s' % response.json()['message'])  # noqa
    result = 'Your comment is published on ' + response.json()['link']
    print(result)
    return result


def readFromFile(filename):
    '''Function to open file, read all the lines from the file sequentially.
    All lines are returned as a list.

    Args: filename - should be a non-binary file in the local directory.

    Returns: filelines; all lines in file as a list.
    '''
    with open(f'{filename}', 'r') as readfile:
        filelines = [line for line in readfile.readlines()]
    readfile.close()
    return filelines


def getHeader(filelines):
    '''Function to get the header of a list of filelines passed through to it.
    Assumes header is the first line of the file.

    Args: filelines - produced by readFromfile function.

    Returns: first line in file as a string value.
    '''
    if len(filelines) > 0:
        return filelines[0]
    else:
        print("File is empty.")


def getBody(filelines):
    '''Function to get the body of a list os filelines passed through to it.
    Assumes body is everything after the first line.

    Args: filelines - produced by readFromfile function.

    Returns: all lines in file, joined together as a string value.
    '''
    if len(filelines) > 0:
        body = filelines[1:]
        return ''.join(body)
    else:
        print("File is empty.")

# Some Example calls:
# alllines = readFromFile('testfile')

# header = getHeader(alllines)
# print(f'Header is: {header}')
# body = getBody(alllines)
# print(f'Body is: {body}')


if __name__ == "__main__":  # pragma: no cover
    PARSER.add_argument("command",
                        help=f"Command you want to run; Options include: {', '.join(ACCEPT)}.",  # noqa
                        type=str)
    PARSER.add_argument("--title",
                        help="Used in conjunction with the upload command; specifies the title of the blog post.",  # noqa
                        type=str)
    PARSER.add_argument("--body",
                        help="Used in conjunction with the upload command; specifies the body of the blog post.",  # noqa
                        type=str)
    PARSER.add_argument("--post_id",
                        help="Used in conjunction with the comment command; specifies the post id to which the comment should be posted.",  # noqa
                        type=str)
    PARSER.add_argument("--comment",
                        help="Used in conjunction with the comment command; specifies the comment to be posted.",  # noqa
                        type=str)
    PARSER.add_argument("--file",
                        help="Used in conjunction with the fileload command; specifies the file containing details of the blog post.",  # noqa
                        type=str)
    ARGS = PARSER.parse_args()
    PARSER = argparse.ArgumentParser()

    if ARGS.command.lower() not in ACCEPT:
        raise ValueError(f"Command needs to be one of the following: {', '.join(ACCEPT)}")  # noqa

    if ARGS.command.lower() == "show":
        print("Command is show")
        read_blog_posts()
    elif ARGS.command.lower() == "upload":
        print("Command is upload")
        print(f"This is the title of the blog post: {ARGS.title}")
        print(f"This is the body of the blog post: {ARGS.body}")
        write_blog_post(title=ARGS.title, content=ARGS.body)
    elif ARGS.command.lower() == "comment":
        print("Command is comment")
        print(f"Make comment to this post id: {ARGS.post_id}")
        print(f"This is the comment: {ARGS.comment}")
        write_blog_comment(post_id=ARGS.post_id, comment=ARGS.comment)
    else:
        print("Command is fileload")
        header = getHeader(readFromFile(ARGS.file))
        content = getBody(readFromFile(ARGS.file))
        print(f"This is the title of the blog post: {header}")
        print(f"This is the body of the blog post: {content}")
        write_blog_post(title=header, content=content)
# temporary code to test env.os
    # print(WORDPRESS_USERNAME, WORDPRESS_PASSWORD, WORDPRESS_URL)
    # read_blog_posts(num_posts=5)
    # get_all_posts()
    # write_blog_comment(23, 'This comment is from the CLI, how cool is that?')
