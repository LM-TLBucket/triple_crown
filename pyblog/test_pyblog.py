'''This module holds the test cases for the Triple Crowm Wordpress App
'''
import html
import json
import re
import unittest
import unittest.mock

import pyblog


class MockResponse:

    def __init__(self, text, status_code):
        self.text = text
        self.status_code = status_code

    def json(self):
        return json.loads(self.text)

    def __iter__(self):
        return self

    def __next__(self):
        return self


def mock_requests_get_all_posts(*args, **kwargs):
    text = '''[
        {
            "id": 1,
            "date": "2020-03-04T16:05:37",
            "date_gmt": "2020-03-04T16:05:37",
            "guid": {
                "rendered": "http://18.188.92.239:8088/?p=1"
            },
            "modified": "2020-03-04T16:05:37",
            "modified_gmt": "2020-03-04T16:05:37",
            "slug": "hello-world",
            "status": "publish",
            "type": "post",
            "link": "http://18.188.92.239:8088/2020/03/04/hello-world/",
            "title": {
                "rendered": "Hello world!"
            },
            "content": {
                "rendered": "\\n<p>Hello, Welcome to WordPress!</p>\\n",
                "protected": false
            },
            "excerpt": {
                "rendered": "<p>Hello, Welcome to WordPress!</p>\\n",
                "protected": false
            },
            "author": 1,
            "featured_media": 0,
            "comment_status": "open",
            "ping_status": "open",
            "sticky": false,
            "template": "",
            "format": "standard",
            "meta": [],
            "categories": [
                1
            ],
            "tags": [],
            "_links": {
                "self": [
                    {
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/posts/1"
                    }
                ],
                "collection": [
                    {
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/posts"
                    }
                ],
                "about": [
                    {
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/types/post"
                    }
                ],
                "author": [
                    {
                    "embeddable": true,
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/users/1"
                    }
                ],
                "replies": [
                    {
                    "embeddable": true,
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/comments?post=1"
                    }
                ],
                "version-history": [
                    {
                    "count": 0,
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/posts/1/revisions"
                    }
                ],
                "wp:attachment": [
                    {
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/media?parent=1"
                    }
                ],
                "wp:term": [
                    {
                    "taxonomy": "category",
                    "embeddable": true,
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/categories?post=1"
                    },
                    {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "http://18.188.92.239:8088/wp-json/wp/v2/tags?post=1"
                    }
                ],
                "curies": [
                    {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                    }
                ]
            }
        },
        {
            "id": 3,
            "date": "2020-03-04T16:15:44",
            "date_gmt": "2020-03-04T16:15:44",
            "guid": {
                "rendered": "http://18.188.92.239:8088/?p=3"
            },
            "modified": "2020-03-04T16:15:44",
            "modified_gmt": "2020-03-04T16:15:44",
            "slug": "hello-world-what",
            "status": "publish",
            "type": "post",
            "link": "http://18.188.92.239:8088/2020/03/04/hello-world-what/",
            "title": {
                "rendered": "Hello world, What!"
            },
            "content": {
                "rendered": "\\n<p>Welcome to WordPress, WHAT!?</p>\\n",
                "protected": false
            },
            "excerpt": {
                "rendered": "<p>Welcome to WordPress, WHAT!?</p>\\n",
                "protected": false
            },
            "author": 1,
            "featured_media": 0,
            "comment_status": "open",
            "ping_status": "open",
            "sticky": false,
            "template": "",
            "format": "standard",
            "meta": [],
            "categories": [
                1
            ],
            "tags": [],
            "_links": { }
        },
                {
            "id": 5,
            "date": "2020-03-04T17:33:33",
            "date_gmt": "2020-03-04T17:33:33",
            "guid": {
                "rendered": "http://18.188.92.239:8088/?p=5"
            },
            "modified": "2020-03-04T17:33:33",
            "modified_gmt": "2020-03-04T17:33:33",
            "slug": "New post, Read it Here!",
            "status": "publish",
            "type": "post",
            "link": "http://18.188.92.239:8088/2020/03/04/new-post-read/",
            "title": {
                "rendered": "Finally, a new post to read!"
            },
            "content": {
                "rendered": "\\n<p>Finally, a new post to read!</p>\\n",
                "protected": false
            },
            "excerpt": {
                "rendered": "<p>Finally, a new post to read!</p>\\n",
                "protected": false
            },
            "author": 1,
            "featured_media": 0,
            "comment_status": "open",
            "ping_status": "open",
            "sticky": false,
            "template": "",
            "format": "standard",
            "meta": [],
            "categories": [
                1
            ],
            "tags": [],
            "_links": { }
        }
    ]
    '''  # noqa
    response = MockResponse(text, 200)
    return response


def mock_requests_failed_response(*args, **kwargs):
    text = '''
    {   'code': 'rest_no_route',
        'message': 'No route was found matching the URL and request method',
        'data': {'status': 404}
    }
    '''
    response = MockResponse(text, 404)
    return response


def mock_requests_write_post(*args, **kwargs):
    text = '''
    {
        "id": 1,
        "date": "2020-03-04T16:05:37",
        "date_gmt": "2020-03-04T16:05:37",
        "guid": {
            "rendered": "http://18.188.92.239:8088/?p=1"
        },
        "modified": "2020-03-04T16:05:37",
        "modified_gmt": "2020-03-04T16:05:37",
        "slug": "hello-world",
        "status": "publish",
        "type": "post",
        "link": "http://18.188.92.239:8088/2020/03/04/hello-world/",
        "title": {
            "rendered": "Hello world!"
        },
        "content": {
            "rendered": "\\n<p>Hello James!</p>\\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>Hello James!</p>\\n",
            "protected": false
        },
        "author": 1,
        "featured_media": 0,
        "comment_status": "open",
        "ping_status": "open",
        "sticky": false,
        "template": "",
        "format": "standard",
        "meta": [],
        "categories": [
            1
        ],
        "tags": [],
        "_links": { }
    }
    '''
    response = MockResponse(text, 201)
    return response


def mock_requests_write_post_failed(*args, **kwargs):
    text = '''
    {   'code': 'rest_no_route',
        'message': 'No route was found matching the URL and request method',
        'data': {'status': 404}
    }
    '''
    response = MockResponse(text, 404)
    return response


def mock_requests_write_comment(*args, **kwargs):
    text = '''
    {
        "id": 10,
        "post": 23,
        "parent": 0,
        "author": 1,
        "author_name": "wpuser",
        "author_email": "bob@bob.com",
        "author_url": "",
        "author_ip": "3.230.11.92",
        "author_user_agent": "PostmanRuntime/7.22.0",
        "date": "2020-03-09T13:18:22",
        "date_gmt": "2020-03-09T13:18:22",
        "content": {
            "rendered": "<p>This is only a test comment!</p>",
            "raw": "This is only a test comment!"
        },
        "link": "http://18.188.92.239:8088/2020/03/09/test-post-heading/#comment-10",
        "status": "approved",
        "type": "comment",
        "author_avatar_urls": {
            "24": "http://2.gravatar.com/avatar/820d0e4ee14e986a44d33782ca852f51?s=24&d=mm&r=g",
            "48": "http://2.gravatar.com/avatar/820d0e4ee14e986a44d33782ca852f51?s=48&d=mm&r=g",
            "96": "http://2.gravatar.com/avatar/820d0e4ee14e986a44d33782ca852f51?s=96&d=mm&r=g"
        },
        "meta": [],
        "_links": { }
    }
    '''  # noqa
    response = MockResponse(text, 201)
    return response


def mock_requests_write_comment_failed(*args, **kwargs):
    text = '''
    {
        "code": "rest_comment_invalid_post_id",
        "message": "Sorry, you are not allowed to create this comment without a post.",
        "data": {
            "status": 403
        }
    }    
    '''  # noqa
    response = MockResponse(text, 403)
    return response


class TestPyblog(unittest.TestCase):

    @unittest.mock.patch('requests.get', mock_requests_get_all_posts)
    def test_get_all_posts(self):
        list_all_posts = pyblog.get_all_posts()
        self.assertEqual(len(list_all_posts), 3)

    @unittest.mock.patch('requests.get', mock_requests_failed_response)
    def test_get_all_posts_failed(self):
        with self.assertRaises(ValueError):
            pyblog.get_all_posts()

    @unittest.mock.patch('requests.get', mock_requests_get_all_posts)
    def test_read_blog_posts_default(self):
        blog_post = pyblog.read_blog_posts()
        expected = ['id: 1\ndate: 2020-03-04T16:05:37\ntitle: Hello world!\ncontents: \nHello, Welcome to WordPress!\n']  # noqa
        self.assertEqual(expected, blog_post)

    @unittest.mock.patch('requests.get', mock_requests_get_all_posts)
    def test_read_blog_posts_two(self):
        blog_post = pyblog.read_blog_posts(num_posts=2)
        expected = ['id: 1\ndate: 2020-03-04T16:05:37\ntitle: Hello world!\ncontents: \nHello, Welcome to WordPress!\n',  # noqa
                    'id: 3\ndate: 2020-03-04T16:15:44\ntitle: Hello world, What!\ncontents: \nWelcome to WordPress, WHAT!?\n']  # noqa
        self.assertEqual(expected, blog_post)

    @unittest.mock.patch('requests.get', mock_requests_get_all_posts)
    def test_read_blog_posts_greater_than_available(self):
        blog_post = pyblog.read_blog_posts(num_posts=5)
        expected = ['id: 1\ndate: 2020-03-04T16:05:37\ntitle: Hello world!\ncontents: \nHello, Welcome to WordPress!\n',  # noqa
                    'id: 3\ndate: 2020-03-04T16:15:44\ntitle: Hello world, What!\ncontents: \nWelcome to WordPress, WHAT!?\n',  # noqa
                    'id: 5\ndate: 2020-03-04T17:33:33\ntitle: Finally, a new post to read!\ncontents: \nFinally, a new post to read!\n']  # noqa
        self.assertEqual(expected, blog_post)

    @unittest.mock.patch('requests.get', mock_requests_get_all_posts)
    def test_get_blog_post_details(self):
        list_all_posts = pyblog.get_all_posts()
        post_id = list_all_posts[0]['id']
        date = list_all_posts[0]['date']
        title = html.unescape(list_all_posts[0]['title']['rendered'])
        contents = re.sub(r'\<[^>]*>', r'', html.unescape(list_all_posts[0]['content']['rendered']).rstrip())  # noqa
        self.assertEqual(post_id, 1)
        self.assertEqual(date, '2020-03-04T16:05:37')
        self.assertEqual(title, 'Hello world!')
        self.assertEqual(contents, '\nHello, Welcome to WordPress!')

    @unittest.mock.patch('requests.post', mock_requests_write_post)
    def test_write_blog_post(self):
        result = pyblog.write_blog_post(title='Post made via command line', content='This is the body of my post which was passed via command line.')  # noqa
        expected = 'Your post is published on http://18.188.92.239:8088/2020/03/04/hello-world/'  # noqa
        self.assertEqual(expected, result)

    @unittest.mock.patch('requests.post', mock_requests_failed_response)
    def test_write_blog_post_failed(self):
        with self.assertRaises(ValueError):
            pyblog.write_blog_post(title='Post made via command line', content='This is the body of my post which was passed via command line.')  # noqa

    @unittest.mock.patch('requests.post', mock_requests_write_comment)
    def test_write_blog_comment(self):
        result = pyblog.write_blog_comment(post_id='23', comment='This is only a test comment!')  # noqa
        expected = 'Your comment is published on http://18.188.92.239:8088/2020/03/09/test-post-heading/#comment-10'  # noqa
        self.assertEqual(expected, result)

    @unittest.mock.patch('requests.post', mock_requests_write_comment_failed)
    def test_write_blog_comment_failed(self):
        with self.assertRaises(ValueError):
            pyblog.write_blog_comment(post_id='9999', comment='This should fail.')  # noqa

    def test_read_from_file(self):
        result = pyblog.readFromFile(filename='pyblog/post_testfile.txt')
        expected = ['Test Post Heading from post_testfile.txt\n', '\n',
                    'This is a test file to be used for testing the fileload command of the pyblog.py script.\n', '\n',  # noqa
                    'This file is critical in the comprehensive test coverage of the pyblog.py script.  '  # noqa
                    'How could we ever fully test the pyblog.py script without this file?  If you are reading this post then clearly this file did it\'s job.\n',  # noqa
                    '\n', 'Please leave a comment below to express your appreciation of how the post_testfile.txt has supported our testing efforts.\n']  # noqa
        self.assertEqual(expected, result)

    def test_get_header(self):
        file_lines = ['Test Post Heading from post_testfile.txt\n', '\n',
                    'This is a test file to be used for testing the fileload command of the pyblog.py script.\n', '\n',  # noqa
                    'This file is critical in the comprehensive test coverage of the pyblog.py script.  '  # noqa
                    'How could we ever fully test the pyblog.py script without this file?  If you are reading this post then clearly this file did it\'s job.\n',  # noqa
                    '\n', 'Please leave a comment below to express your appreciation of how the post_testfile.txt has supported our testing efforts.\n']  # noqa
        result = pyblog.getHeader(file_lines)
        expected = file_lines[0]
        self.assertEqual(expected, result)

    def test_get_body(self):
        file_lines = ['Test Post Heading from post_testfile.txt\n', '\n',
                    'This is a test file to be used for testing the fileload command of the pyblog.py script.\n', '\n',  # noqa
                    'This file is critical in the comprehensive test coverage of the pyblog.py script.  '  # noqa
                    'How could we ever fully test the pyblog.py script without this file?  If you are reading this post then clearly this file did it\'s job.\n',  # noqa
                    '\n', 'Please leave a comment below to express your appreciation of how the post_testfile.txt has supported our testing efforts.\n']  # noqa
        result = pyblog.getBody(file_lines)
        expected = ''.join(file_lines[1:])
        self.assertEqual(expected, result)

    def test_get_header_emptyfile(self):
        file_lines = []
        result = pyblog.getHeader(file_lines)
        expected = None
        self.assertEqual(expected, result)

    def test_get_body_emptyfile(self):
        file_lines = []
        result = pyblog.getBody(file_lines)
        expected = None
        self.assertEqual(expected, result)
