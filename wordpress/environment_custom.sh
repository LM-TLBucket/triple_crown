#!/bin/bash

echo "agent.applicationName = $APPNAME" >> /usr/local/etc/php/conf.d/appdynamics_agent.ini
echo "agent.tierName = $INSTANCETIER" >> /usr/local/etc/php/conf.d/appdynamics_agent.ini
echo "agent.nodeName = $NODENAME" >> /usr/local/etc/php/conf.d/appdynamics_agent.ini
echo "agent.accountName = $APPDUSER" >> /usr/local/etc/php/conf.d/appdynamics_agent.ini
echo "agent.accountAccessKey = $APPDPASSWORD" >> /usr/local/etc/php/conf.d/appdynamics_agent.ini

docker-entrypoint.sh apache2-foreground
